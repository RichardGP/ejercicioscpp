
#include "pch.h"
#include <iostream>
using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::right;

#include <iomanip>
using std::setprecision;
using std::setw;

#include <cmath>
using std::pow;

#include <string>
using std::string;
#include "Ejercicios.h"
//Ejercicio 5.6
void Ejercicios5 ::forCentinela()
{
	int centinela = 9999;
	int numero;
	int suma=0;
	cout << "Ingrese la secuencia de numeros( o 9999 para salir): ";
	cin >> numero;
	int contador;
	for (contador = 0; numero != centinela; contador++ )
	{
		suma += numero;
		cin >> numero;
	}
	cout << "El promedio de los numero ingresados es: " << setprecision(2) << fixed <<
		static_cast<double>(suma)/contador;
	
}
//Ejercicio 5.8
void Ejercicios5::menorNumero()
{
	int nRestantes;
	int suma = 0;

	cout << "Ingrese una serie de numeros(El primer numero es la cantidad de numeros restantes): ";

	cin >> nRestantes;
	int numero;
	int menor;

	cin >> numero;
	menor = numero;
	for (int control = 1; control < nRestantes; control++)
	{
		numero;
		cin >> numero;
		if (numero < menor)
			menor = numero;		
	
	}

	cout << "El numero menor es:" << menor << endl;
	
}
//Ejercicio 5.9
void Ejercicios5::numerosImpar() 
{
	int producto=1;
	for (int i = 1; i <= 15; i+=2)
	{
		producto *= i;
	}
	cout << "El producto de los enteros impares hasta el 15 es: "
		<< producto;
}

//Ejercicio 5.10
void Ejercicios5 ::factorial()
{ 
	int factorial;
	int numero;
	int espacio=0;
	//cout << "Ingrese un numero del 1 al 5: ";
	//cin >> numero;
	
	cout << "Numero " << setw(4) <<"5" << setw(7) << "4" << setw(7) << "3" << setw(6) << "2" << setw(6) << "1" << endl;
	cout << "Factorial ";
		for (numero = 5; numero > 0 ; numero--) {
			factorial = 1;
			for (int numero2 = numero; numero2 > 0; numero2--)
			{
				factorial *= numero2;
			
			}
			cout << factorial <<setw(6);
			
		}

}

//5.11
void Ejercicios5::interesCompuesto()
{
	double monto;
	double principal = 1000.0;
	int tasa;
	for (tasa = 5; tasa <= 10; tasa++) {
		cout << "Interes compuesto para la tasa del " << tasa << "%"<<endl;
		cout << "Anio" << setw(21) << "Monto en deposito" << endl;
		cout << fixed << setprecision(2);

		for (int anio = 1; anio <= 10; anio++)
		{
			monto = principal * pow(1.0 + (static_cast<double>(tasa)/100), anio);

			cout << setw(4) << anio << setw(21) << monto << endl;
		}
		cout << "\n";
	}

}

void Ejercicios5::repaso()
{
	string valor = "La potencia es ";
	int numerador = 10;
	int denominador = 5;
	int potencia = 5;
	double potencia2 = pow(static_cast<double>(numerador) / denominador, potencia);
	cout << fixed << setprecision(4);
	cout << valor << setw(5) << potencia2 << endl;
}
//Ejercicio 5.12
void Ejercicios5::patronesCiclo()
{
	//int i;
	//int j;
	for (int i = 1; i <= 10; i++) {
		
		for (int j = 1; j <= i; j++)
		{
			cout << "*";
		}
		
		cout << endl;
	}

	cout << "\n";
	for (int i = 10; i >= 1; i--) {

		for (int j = 1; j <= i; j++)
		{
			cout << "*";
		}

		cout << endl;
	}
	cout << "\n";
	
	int espacios=1;
	for (int i = 10; i >= 1; i--) {
		cout << setw(espacios);
		for (int j = 1; j <= i; j++)
		{
			
			cout<<"*";
		}
		espacios++;

		cout << endl;
	}
	cout << "\n";
	
	espacios = 10;
	for (int i = 1; i <= 10; i++) {
		cout << setw(espacios);
		for (int j = 1; j <= i; j++)
		{
			cout << "*";
		}
		espacios--;
		cout << endl;
	}

}

//Ejercicio 5.12 extra
void Ejercicios5::patronesCicloUnidos()
{
	//int i;
	//int j;
	int espacios = 11;
	int espacios2 = 2;
	for (int i = 1; i <= 10; i++) {

		for (int j = 1; j <= i; j++)
		{
			cout << "*";
		}

		cout << setw(espacios);
		for (int j = 10; j >= i; j--)
		{
			cout << "*";
		}
		
		cout << setw(espacios2);
		for (int j = 10; j >= i; j--)
		{

			cout << "*";
		}
		
		cout << setw(espacios);
		for (int j = 1; j <= i; j++)
		{

			cout << "*";
		}

		espacios--;
		espacios2+=2;
		cout << endl;

	}
}

void Ejercicios5::dibujarBarras() 
{
	int numero;

	for (int i = 0; i < 5; i++)
	{
		cout << "Ingrese un numero entre 1 y 30: ";
		cin >> numero;
		for (int j = 0; j < numero; j++) 
		{
			cout << "*";
		}
		cout << endl;

	}
	
}

void Ejercicios5::tiendaProductos()
{
	int numeroProducto;
	int cantidadProducto;
	double precio;
	double totalProducto = 0;
	double total = 0;
	cout << "Ingrese el numero y la cantidad del producto(o -1 para salir): ";
	cin >> numeroProducto;
	cin >> cantidadProducto;

	while (numeroProducto != -1 || cantidadProducto != -1)
	{
		
		switch (numeroProducto)
		{
		case 1:
			precio = 2.98;
			totalProducto = precio * cantidadProducto;
			break;
		case 2:
			precio = 4.5;
			totalProducto = precio * cantidadProducto;
			break;
		case 3:
			precio = 9.98;
			totalProducto = precio * cantidadProducto;
			break;
		case 4:
			precio = 4.49;
			totalProducto = precio * cantidadProducto;
			break;
		case 5:
			precio = 6.87;
			totalProducto = precio * cantidadProducto;
			break;
		case ' ':
			break;
		default:
			cout << "El producto ingresado no existe " << endl;
			break;
		}

		total += totalProducto;

		cout << "Ingrese el numero y la cantidad del producto(o EOF para salir): ";
		cin >> numeroProducto;
		cin >> cantidadProducto;
	}

	cout << "El total precio total por todos los productos es: " << total << endl;
}

void Ejercicios5::valorPi() 
{
	double pi = 0;
	int numero = 0;
	double denominador = 1;
	double denominador2 = 3;
	cout << "Numero" << setw(10) << "Pi" << endl;
	//cout << setprecision(4) << fixed;
	/*for (int i = 0; i < 500; i++)
	{
		numero++;
		pi += 4 / denominador;
		denominador += 4;
		cout << numero << setw(14) << pi << endl;
		
		numero++;
		pi -= 4 / denominador2;
		denominador2 += 4;
		cout << numero << setw(14) << pi << endl;
		
	}*/
	//cout << setprecision(6) << fixed << pi;
	
	
	cout << setprecision(4) << fixed;
	for (int i = 1; i <= 500; i++)
	{
		pi += 4 / (denominador) - 4 / (denominador2);
		
		denominador += 4;
		denominador2 += 4;

		cout << i << setw(14 ) << pi << endl;

	}
}

void Ejercicios5::triplePitagoras()
{

	for (int i = 1; i <= 500; i++)
	{
		for (int j = 1; j <= 500; j++) 
		{
			for (int k = 1; k <= 500; k++)
			{
				if (pow(i, 2) == pow(j, 2) + pow(k, 2))
					//cout << pow(i, 2) << " = " << pow(j, 2) << " + " << pow(k, 2) << endl;
					cout << "Hipotenusa " << i << " Lado1 " << j << " Lado2 " << k << endl;
			}
		}
	}
}

void Ejercicios5::sueldoDeEmpleados()
{
	int codigoEmpleado;
	int sueldo;
	

	cout << "Sueldo de empleados" << endl;
	cout << "\n1. Gerente "
		<< "\n2. Trabajador por hora "
		<< "\n3. Trabajador por comision "
		<< "\n4. Trabajador por pieza "
		<< "\nIntroduzca el codigo del empleado(o -1): ";
	cin >> codigoEmpleado;
	cout << "\n";

	while (codigoEmpleado != -1) {

		switch (codigoEmpleado)
		{
		case 1:

			cout << "Introduzca el sueldo semanal del empleado: ";
			cin >> sueldo;
			cout << "El sueldo del empleado es: " << sueldo << endl;
			break;

		case 2:
			int horas;
			int horasExtra;
			int sueldoExtra;
			int sueldoHora;
			cout << "Introduzca el sueldo por hora del empleado: ";
			cin >> sueldoHora;
			cout << "Introduzca las horas trabajadas: ";
			cin >> horas;
			if (horas > 40)
			{
				horasExtra = horas - 40;
				sueldoExtra = (sueldoHora*horasExtra)*1.5;
				sueldo = (sueldoHora * 40) + sueldoExtra;
				cout << "El sueldo del empleado es: " << sueldo << endl;;
			}
			else
			{
				sueldo = sueldoHora * horas;
				cout << "El sueldo del empleado es: " << sueldo << endl;
			}
			break;
		case 3:
			int ventas;
			int comision;
			cout << "Introduzca las ventas totales por semana del empleado: ";
			cin >> ventas;
			comision = ventas * 0.057;
			sueldo = 250 + comision;

			cout << "El sueldo del empleado es: " << sueldo << endl;
			break;
		case 4:
			int sueldoPorArticulo;
			int cantidadArticulos;
			cout << "Ingrese el sueldo por articulos producidos: ";
			cin >> sueldoPorArticulo;
			cout << "Ingrese la cantidad de articulos producidos: ";
			cin >> cantidadArticulos;
			sueldo = sueldoPorArticulo * cantidadArticulos;

			cout << "El sueldo del empleado es: " << sueldo << endl;
			break;

		default:
			cout << "El codigo de empleado ingresado no existe " << endl;
			break;
		}

		cout << "\n\n";
		cout << "\n1. Gerente "
			<< "\n2. Trabajador por hora "
			<< "\n3. Trabajador por comision "
			<< "\n4. Trabajador por pieza "
			<< "\nIntroduzca el codigo del empleado(o -1): ";
		cin >> codigoEmpleado;

	}
}

void Ejercicios5::leyesDeMorgan()
{
	int x = 5;
	int y = 6;
	int a = 3;
	int b = 3;
	int g = 4;
	int i = 10; 
	int j = 11;


	if ((!(x < 5) && !(y >= 7)) == !(x < 5 || y >= 7))
	{
		cout << "Las expresiones (!(x < 5) && !(y >= 7)) y !(x < 5 || y >= 7) son equivalentes " << endl;
	}
	else 
	{
		cout << "Las expresiones no son equivalente " << endl;
	}

	if ((!(a == b) && !(g != 5)) == !((a == b) || (g != 5)))
	{
		cout << "Las expresiones (!(a == b) && !(g != 5)) y !((a == b) || (g != 5)) son equivalentes " << endl;

	}
	else
	{
		cout << "Las expresiones no son equivalente " << endl;
	}
	
	if (!((x <= 8) && (y > 4)) == (!(x <= 8) || !(y > 4)))
	{
		cout << "Las expresiones !((x <= 8) && (y > 4)) y (!(x <= 8) || !(y > 4)) son equivalentes " << endl;
	}
	else
	{
		cout << "Las expresiones no son equivalente " << endl;
	}
	if (!((i > 4) || (j <= 6)) == (!(i > 4) && !(j <= 6)))
	{
		cout << "Las expresiones !((i > 4) || (j <= 6)) y (!(i > 4) && !(j <= 6)) son equivalentes " << endl;
	}
	else 
	{
		cout << "Las expresiones no son equivalente " << endl;
	}
}


/*void Ejercicios5::ejercicioRombo()
{
	int espacios = 5;
	int espacios2 = 2;

	for(int i = 1; i <= 9; i+=2) 
	{
		cout << setw(espacios);
		for (int j = 1; j <= i; j++)
		{
			
			cout << "*";
		}
		cout << endl;

		espacios--;

	}

	for (int k = 7; k >= 1; k-= 2)
	{
		cout << setw(espacios2);
		for (int j = 1; j <= k; j++)
		{

			cout << "*";
		}
		cout << endl;

		espacios2++;

	}
}*/

void Ejercicios5::ejercicioRombo()
{
	int numero = 19;
	int espacios = (numero+ 2) / 2;

	for (int i = 0; i < numero+1; i++)
	{
		if (i < espacios) {
			for (int j = 0; j < espacios - i ; j++)
			{
				cout << " ";
			}
			for (int k = 0; k < (i * 2) - 1; k++)
			{
				cout << "*";
			}
			cout << endl;
		}
		else if (i == espacios)
		{
			for (int k = 0; k < (i * 2) - 1; k++)
			{
				cout << "*";
			}
			cout << endl;
		}
		else
		{
			for (int j = i; j > espacios; j--)
			{
				cout << " ";
			}
			for (int k = numero*2; k > (i * 2) - 1  ; k--)
			{
				cout << "*";
			}
			cout << endl;

		}
	}
	
}

void Ejercicios5::ejercicioRomboUsuario()
{
	int numero;
	cout << "Ingrese un numero del 1 a 19 impar para dibujar el rombo: ";
	cin >> numero;
	if (numero % 2 == 0)
	{
		cout << "El numero ingresado no es impar" << endl;
	}
	else
	{
		int espacios = (numero + 2) / 2;

		for (int i = 0; i < numero + 1; i++)
		{
			if (i < espacios) {
				for (int j = 0; j < espacios - i; j++)
				{
					cout << " ";
				}
				for (int k = 0; k < (i * 2) - 1; k++)
				{
					cout << "*";
				}
				cout << endl;
			}
			else if (i == espacios)
			{
				for (int k = 0; k < (i * 2) - 1; k++)
				{
					cout << "*";
				}
				cout << endl;
			}
			else
			{
				for (int j = i; j > espacios; j--)
				{
					cout << " ";
				}
				for (int k = numero * 2; k > (i * 2) - 1; k--)
				{
					cout << "*";
				}
				cout << endl;

			}
		}
	}
}

void Ejercicios5::remplazoBreak()
{
	for (int cuenta = 1; cuenta <= 10; cuenta++)
	{
		if (cuenta == 5)
			cuenta = 10;
		else
			cout << cuenta << " ";
	}
	cout << endl;
	
	/*for (int i = 1; i <= 5; i++)
	{
		for (int j = 1; j <= 3; j++)
			 {
			 for (int k = 1; k <= 4; k++)
				 cout << '*';
			
				 cout << endl;
			} // fin del for interior
		
			 cout << endl;
		 } // fin del for exterior
		 */
}

void Ejercicios5::doceDiasDeNavidad()
{

	for (int dia = 1; dia <= 12; dia++)
	{
		switch (dia)
		{
		case 1: cout << "El primer dia de Navidad,\n";
			break;
		case 2:	cout << "El segundo dia de Navidad,\n";
			break;
		case 3:	cout << "El tercer dia de Navidad,\n";
			break;
		case 4:	cout << "El cuarto dia de Navidad,\n";
			break;
		case 5:	cout << "El quinto dia de Navidad,\n";
			break;
		case 6: cout << "El sexto dia de Navidad,\n";
			break;
		case 7: cout << "El septimo dia de Navidad,\n";
			break;
		case 8: cout << "El octavo dia de Navidad,\n";
			break;
		case 9: cout << "El noveno dia de Navidad,\n";
			break;
		case 10: cout << "El decimo dia de Navidad,\n";
			break;
		case 11: cout << "El undecimo dia de Navidad,\n";
			break;
		case 12: cout << "El doudecimo dia de Navidad,\n";
			break;
		default:
			break;
		}

		cout << "mi verdadero amor me envio" << endl;

		for (int i = dia; i >= 1; i--)
		{
			switch (i)
			{
			case 1: cout << "Una perdiz en un peral.\n";
				break;
			case 2: cout << "Dos tortolas\ny ";
				break;
			case 3: cout << "Tres gallinas francesas,\n";
				break;
			case 4: cout << "Cuatro pajaros llamando,\n";
				break;
			case 5: cout << "Cinco anillos de oro,\n";
				break;
			case 6: cout << "Seis ocas poniendo,\n";
				break;
			case 7: cout << "Siete cisnes nadando,\n";
				break;
			case 8: cout << "Ocho criadas ordeniando,\n";
				break;
			case 9: cout << "Nueve senioras bailando,\n";
				break;
			case 10: cout << "Diez seniores brincando,\n";
				break;
			case 11: cout << "Once gaiteros tocando,\n";
				break;
			case 12: cout << "Doce tamborileros tamboreando,\n";
				break;

			default:
				break;
			}
		}
		cout << "\n \n";
	}
}


void Ejercicios5:: problemaDePeterMinuit()
{
	double monto;
	double principal = 24.00;
	double tasa = 5;

	cout << "Anio" << setw(27) << "Monto en deposito" << endl;

	cout << fixed << setprecision(2);

	for (tasa; tasa <= 10; tasa++)
	{
		for (int anio = 1; anio <= 381; anio++)
		{
			monto = principal * pow(1 + (tasa / 100 ), anio);

			cout << setw(4) << anio << setw(27) << monto << endl;

		}
		cout << "La tasa es "<<tasa << endl;
	}
}


void Ejercicios5::operadorTernario() {

	int val = !true ? 1 : 0;

	cout << "El resultado es: " << val << endl;

}