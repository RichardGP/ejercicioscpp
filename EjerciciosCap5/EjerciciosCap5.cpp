// EjerciciosCap5.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//

#include "pch.h"
#include <iostream>

#include "Ejercicios.h"

int main()
{
	Ejercicios5 ejercicios;
	//ejercicios.forCentinela();
	//ejercicios.menorNumero();
	//ejercicios.numerosImpar();
	//ejercicios.factorial();
	//ejercicios.interesCompuesto();
	//ejercicios.patronesCiclo();
	//ejercicios.patronesCicloUnidos();
	//ejercicios.dibujarBarras();
	//ejercicios.tiendaProductos();
	//ejercicios.valorPi();
	//ejercicios.triplePitagoras();
	//ejercicios.sueldoDeEmpleados();
	//ejercicios.leyesDeMorgan();
	//ejercicios.ejercicioRombo();
	//ejercicios.ejercicioRomboUsuario();
	//ejercicios.remplazoBreak();
	ejercicios.doceDiasDeNavidad();
	//ejercicios.problemaDePeterMinuit();
	//ejercicios.repaso();
	//ejercicios.operadorTernario();
	return 0;
}
