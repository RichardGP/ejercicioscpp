#pragma once

//Ejercicios
class Ejercicios5
{
public:
	void forCentinela();
	void menorNumero();
	void numerosImpar();
	void factorial();
	void interesCompuesto();
	void patronesCiclo();
	void patronesCicloUnidos();
	void dibujarBarras();
	void tiendaProductos();
	void valorPi();
	void triplePitagoras();
	void sueldoDeEmpleados();
	void leyesDeMorgan();
	void ejercicioRombo();
	void ejercicioRomboUsuario();
	void remplazoBreak();
	void doceDiasDeNavidad();
	void problemaDePeterMinuit();
	void repaso();
	void operadorTernario();
};